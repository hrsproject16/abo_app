import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: Text("Profile"),
        ),

        body: ListView(
          children: <Widget>[
            SafeArea(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      radius: 80,
                      backgroundImage: AssetImage('images/protocoder.png'),
                    ),
                    Text(
                      'Heris Pambudi Susilo',
                      style: TextStyle(
                        fontFamily: 'SourceSansPro',
                        fontSize: 25,
                      ),
                    ),
                    Text(
                      'nomer id',
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: 'SourceSansPro',
                        color: Colors.red[400],
                        letterSpacing: 2.5,
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                      width: 200,
                      child: Divider(
                        color: Colors.teal[100],
                      ),
                    ),
                    Text("Keep visiting protocoderspoint.com for more contents"),
                    Card(
                        color: Colors.white,
                        margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                        child: ListTile(
                          leading: Icon(
                            Icons.phone,
                            color: Colors.teal[900],
                          ),
                          title: Text(
                            '+62 1234567890',
                            style:
                            TextStyle(fontFamily: 'BalooBhai', fontSize: 20.0),
                          ),
                        )),
                    Card(
                      color: Colors.white,
                      margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                      child: ListTile(
                        leading: Icon(
                          Icons.pin_drop,
                          color: Colors.teal[900],
                        ),
                        title: Text(
                          'Alamat',
                          style: TextStyle(fontSize: 20.0, fontFamily: 'Neucha'),
                        ),
                      ),
                    ),
                    Card(
                      color: Colors.white,
                      margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                      child: ListTile(
                        leading: Icon(
                          Icons.timeline,
                          color: Colors.teal[900],
                        ),
                        title: Text(
                          'Riwayat',
                          style: TextStyle(fontSize: 20.0, fontFamily: 'Neucha'),
                        ),
                      ),
                    ),
                    Card(
                      color: Colors.white,
                      margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                      child: ListTile(
                        leading: Icon(
                          Icons.date_range,
                          color: Colors.teal[900],
                        ),
                        title: Text(
                          'Kalender obat',
                          style: TextStyle(fontSize: 20.0, fontFamily: 'Neucha'),
                        ),
                      ),
                    ),
                    Card(
                      color: Colors.white,
                      margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                      child: ListTile(
                        leading: Icon(
                          Icons.cake,
                          color: Colors.teal[900],
                        ),
                        title: Text(
                          '08-05-1995',
                          style: TextStyle(fontSize: 20.0, fontFamily: 'Neucha'),
                        ),
                      ),
                    ),
                    Card(
                      color: Colors.white,
                      margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                      child: ListTile(
                        leading: Icon(
                          Icons.home,
                          color: Colors.teal[900],
                        ),
                        title: Text(
                          'Tempat lahir',
                          style: TextStyle(fontSize: 20.0, fontFamily: 'Neucha'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
    ),
    );
  }
}