import 'package:aboapp/src/beranda.dart';
import 'package:flutter/material.dart';

class Edukasi extends StatefulWidget {
  @override
  _EdukasiState createState() => _EdukasiState();
}


class _EdukasiState extends State<Edukasi> {

  List<String> jeniskelamin=["Laki-laki","Perempuan"];
  String _jeniskelamin="Laki-laki";

  void piljeniskelamin(String value){
    setState(() {
      _jeniskelamin=value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: Text("Konsultasi"),
        ),

        body: new ListView(
          children: <Widget>[
            new Container(
              padding: new EdgeInsets.all(15.0),
              child: new Column(
                children: <Widget>[

                  new Row(
                    children: <Widget>[
                      Text("Silakan Isi data Diri anda",
                        style:
                        new TextStyle(fontSize: 17.0),
                      )
                    ],
                  ),

                  new Padding(padding: new EdgeInsets.all(15.0),),

                  new TextField(
                    decoration: new InputDecoration(
                        hintText: "Nama",
                        labelText: "Nama",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(15.0)
                        )
                    ),
                  ),

                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new Row(
                    children: <Widget>[
                      new Text("Jenis Kelamin", style: new TextStyle(fontSize: 15.0),),

                      new Padding(padding: new EdgeInsets.all(15.0),),

                      new DropdownButton(
                        onChanged: (String value){
                          piljeniskelamin(value);
                        },
                        value: _jeniskelamin,
                        items: jeniskelamin.map((String value){
                          return new DropdownMenuItem(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new TextField(
                    decoration: new InputDecoration(
                        hintText: "Usia",
                        labelText: "Usia",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(15.0)
                        )
                    ),
                  ),

                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new TextField(
                    decoration: new InputDecoration(
                        hintText: "Alamat",
                        labelText: "Alamat",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(15.0)
                        )
                    ),
                  ),

                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new TextField(
                    decoration: new InputDecoration(
                        hintText: "Riwayat Penyakit",
                        labelText: "Riwayat Penyakit",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(15.0)
                        )
                    ),
                  ),

                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new TextField(
                    decoration: new InputDecoration(
                        hintText: "Tekanan Darah",
                        labelText: "Tekanan Darah",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(15.0)
                        )
                    ),
                  ),

                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new TextField(
                    decoration: new InputDecoration(
                        hintText: "Alergi Obat",
                        labelText: "Alergi Obat",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(15.0)
                        )
                    ),
                  ),

                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new TextField(
                    decoration: new InputDecoration(
                        hintText: "Obat yang di konsumsi",
                        labelText: "Obat yang di konsumsi",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(15.0)
                        )
                    ),
                  ),

                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new TextField(
                    maxLines: 5,
                    decoration: new InputDecoration(
                        hintText: "Keluhan Anda",
                        labelText: "Keluhan Yang Dirasakan",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(15.0)
                        )
                    ),
                  ),

                  new Padding(padding: new EdgeInsets.only(top: 15.0),),

                  new Row(
                    children: <Widget>[
                      new RaisedButton(
                        child: new Text("Batal"),
                        color: Colors.redAccent,
                        onPressed: (){
                          Navigator.pop(context);
                        },
                      ),

                      new Padding(padding: new EdgeInsets.all(15.0),),

                      new RaisedButton(
                        child: new Text("OK"),
                        color: Colors.greenAccent,
                        onPressed: (){
                          showDialog(
                              context: context,
                              builder: (BuildContext context){
                                return AlertDialog(
                                  title: Text("Perhatian !!!"),
                                  content: Text('Pastikan data yang anda isi sudah benar ?'),
                                  actions: <Widget>[
                                    new RaisedButton(
                                      child: Text("Batal"),
                                      color: Colors.redAccent,
                                      onPressed: (){
                                        Navigator.pop(context);
                                      },
                                    ),
                                    new RaisedButton(
                                      child: Text("Ok"),
                                      color: Colors.greenAccent,
                                      onPressed: (){
                                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context){
                                          return Beranda();
                                        }));
                                      },
                                    ),
                                  ],
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(2.0))
                                  ),
                                );
                              }
                          );
                        },
                      )
                    ],
                  )



                ],
              ),
            ),
          ],
        ),

      ),
    );
  }
}