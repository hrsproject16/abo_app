
import 'package:aboapp/src/halamanUtama.dart';
import 'package:flutter/material.dart';

class Verefikasi extends StatefulWidget {
  @override
_VerefikasiState createState()=> _VerefikasiState();
}

class _VerefikasiState extends State<Verefikasi>{
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(100),
          child: AppBar(
            title: Text("Verifikasi"),
            backgroundColor: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(15.0),
              ),
            ),
          ),
        ),
          body: ListView(
            children: <Widget>[
              new Container(
                padding: new EdgeInsets.all(15.0),
                child: new Column(
                  children: <Widget>[

                    new Column(
                      children: <Widget>[
                        Text("Silakan pilih Menu di Bawah : ",
                          style:
                          new TextStyle(fontSize: 17.0),
                        )
                      ],
                    ),

                    new Padding(padding: new EdgeInsets.all(50.0),),

                    new Column(
                      children: <Widget>[
                        new ButtonTheme(
                          minWidth: 200.0,
                          height: 40.0,
                          child: RaisedButton(
                            color: Colors.greenAccent,
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context){

                                    return AlertDialog(
                                      title: Text("Catatan !!!!"),
                                      content: Text('Terima kasih, Mohon tunggu 10-30 menit, Notifikasi akan Otomatis Masuk jika pilihan anda sudah di Verifikasi, Jika sudah terverifikasi Silakan Masuk ke Menu Informasi Obat pada beranda'),
                                      actions: <Widget>[

                                        new RaisedButton(
                                          child: Text("Ok"),
                                          color: Colors.greenAccent,
                                          onPressed: (){
                                            Navigator.push(context, MaterialPageRoute(builder: (context){
                                              return HalamanUtama();
                                            }));
                                          },
                                        ),
                                      ],

                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(2.0))
                                      ),
                                    );
                                  }
                              );
                            },
                            child: Text("Resep Baru"),
                          ),
                        ),

                        new Padding(padding: new EdgeInsets.all(15.0),),

                        new ButtonTheme(
                          minWidth: 200.0,
                          height: 40.0,
                          child: RaisedButton(
                            color: Colors.lightBlueAccent,
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context){

                                    return AlertDialog(
                                      title: Text("Catatan !!!!"),
                                      content: Text('Terima kasih, Mohon tunggu 10-30 menit, Notifikasi akan Otomatis Masuk jika pilihan anda sudah di Verifikasi, Jika sudah terverifikasi Silakan Masuk ke Menu Informasi Obat pada beranda'),
                                      actions: <Widget>[

                                        new RaisedButton(
                                          child: Text("Ok"),
                                          color: Colors.greenAccent,
                                          onPressed: (){
                                            Navigator.push(context, MaterialPageRoute(builder: (context){
                                              return HalamanUtama();
                                            }));
                                          },
                                        ),
                                      ],

                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(2.0))
                                      ),
                                    );
                                  }
                              );
                            },
                              child: Text("Resep Ulangan"),
                         ),
                        ),
                      ],
                    ),

                    new Padding(padding: new EdgeInsets.all(60.0),),

                    new Row(
                      children: <Widget>[
                        new RaisedButton(
                            child: new Text("Kembali"),
                            color: Colors.redAccent,
                            onPressed: (){
                              Navigator.pop(context);
                            },
                        ),
                      ],
                    ),

                  ],
                ),
              )
            ],
          ),
      ),
    );
  }
}