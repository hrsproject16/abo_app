import 'package:aboapp/src/halamanUtama.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Pembayaran extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: Text("Pembayaran"),),


        body: Container(
          padding: EdgeInsets.only(top: 300.0),
          child: GridView.count(
            crossAxisCount: 3,
            children: <Widget>[
              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){

                  },
                  splashColor: Colors.green,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                        Text("BRI",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){

                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                        Text("Mandiri",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){

                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                        Text("BCA",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){

                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                        Text("MAndiri",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){

                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                        Text("Alfamaret",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){

                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                        Text("Indomaret",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              new Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      child: Text("Kembali"),
                        color: Colors.redAccent,
                        onPressed: (){
                        Navigator.pop(context);
                        },
                    ),
                  ],
                ),
              ),
              new Container(
                child: Row(
                  children: <Widget>[


                  ],
                ),
              ),

              new Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      child: Text("OK"),
                        color: Colors.greenAccent,
                        onPressed: (){
                          showDialog(
                              context: context,
                              builder: (BuildContext context){
                                return AlertDialog(
                                  title: Text("Terimakasih !!!"),
                                  content: Text('Sudah Melakukan Pembayaran'),
                                  actions: <Widget>[
                                    new RaisedButton(
                                      child: Text("Ok"),
                                      color: Colors.greenAccent,
                                      onPressed: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (context){
                                          return HalamanUtama();
                                        }));
                                      },
                                    ),
                                  ],
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(2.0))
                                  ),
                                );
                              }
                          );

                        },
                    ),
                  ],
                ),
              )



            ],
          ),
        ),
      )
    );
  }
}