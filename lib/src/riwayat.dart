import 'package:aboapp/src/pembayaran.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class Riwayat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget imageSection = Container(
      child: Image.asset('assets/paracetamol.jpg'),
    );

    Widget titleSection = Container(
      padding: EdgeInsets.only(top: 16),
      child: Text(
        'Paracetamol',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18,
        ),
      ),
    );

    Widget descriptionSection = Container(
        padding: EdgeInsets.all(16),
        child: Text(
          'Membantu untuk menurunkan panas dan nyeri,'
              'Efek sampingnya adalah Pusing dan kemerahan namun hal ini jarang terjadi '
              'cara kerjanya :-> memblokir secara peripheral pembentukan '
              'implus nyeri dan menghambat sintesis prostalgladin'' Untuk harganya -> Rp.5.xxx',
          textAlign: TextAlign.justify,
        )
    );


    Widget rateSection = Row(
      children: <Widget>[
        Icon(Icons.star, color: Colors.yellow),
        Icon(Icons.star, color: Colors.yellow),
        Icon(Icons.star, color: Colors.yellow),
        Icon(Icons.star, color: Colors.yellow),
        Icon(Icons.star),
      ],
    );

    Widget reviewSection = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        rateSection,
        Text('170 Reviews'),
      ],
    );

    Widget tombol = Row(
      children: <Widget>[
        RaisedButton(
          child: Text("Kembali"),
            color: Colors.redAccent,
            onPressed: (){
              Navigator.pop(context);
            })
      ],
    );

    Widget tomboll = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        tombol,
        RaisedButton(
          child: Text("Bayar ?"),
            color: Colors.greenAccent,
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return Pembayaran();
              }));
            }),
      ],
    );



    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material Apps',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text(
            'Info Obat',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: ListView(
          children: [
            imageSection,
            titleSection,
            descriptionSection,
            Container(
              padding: EdgeInsets.only(bottom: 24),
              child: reviewSection,
            ),
            Container(
              padding: EdgeInsets.only(bottom: 24),
              child: tomboll,
            )

          ],
        ),
      ),
    );
  }
}

Widget _buildTextSection(
    String text,
    double textSize,
    double paddingTop,
    ) {
  return Container(
    padding: EdgeInsets.only(top: paddingTop),
    child: Text(
      text,
      style: TextStyle(
        fontSize: textSize,
      ),
    ),
  );
}

Widget _buildMenuSection(
    IconData iconData,
    String title,
    String timestamp,
    ) {

  return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
      Icon(iconData),
      _buildTextSection(title, 16, 8),
      _buildTextSection(timestamp, 12, 12),
      ],
  );
}