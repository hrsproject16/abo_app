import 'package:aboapp/src/edukasi.dart';
import 'package:aboapp/src/riwayat.dart';
import 'package:flutter/material.dart';
import 'package:aboapp/src/konseling.dart';
import 'package:aboapp/src/pembayaran.dart';
import 'package:aboapp/src/verifikasi.dart';


class Beranda extends StatefulWidget {
  @override
 _BerandaState createState()=> _BerandaState();
}

class _BerandaState extends State<Beranda>{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(

        appBar: PreferredSize(
          preferredSize: Size.fromHeight(100),
          child: AppBar(
            title: Text("Beranda"),
            backgroundColor: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(15.0),
              ),
            ),
          ),
        ),

        body: Container(
          padding: EdgeInsets.all(15.0),
          child: GridView.count(
              crossAxisCount: 2,
            children: <Widget>[

                Card(
                  elevation:5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                ),
                  margin: EdgeInsets.all(15.0),
                    child: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context){
                          return Konseling();
                        }));
                      },
                        splashColor: Colors.green,
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Icon(
                                Icons.phone,
                                size: 70.0,
                                color: Colors.yellow,
                              ),
                              Text("Pemesanan",style: new TextStyle(fontSize: 17.0))
                            ],
                          ),
                        ),
                      ),
                ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return Verefikasi();
                    }));
                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.verified_user,
                          size: 70.0,
                          color: Colors.blue,
                        ),
                        Text("Verifikasi",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return Pembayaran();
                    }));
                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.attach_money,
                          size: 70.0,
                          color: Colors.green,
                        ),
                        Text("Pembayaran",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return Riwayat();
                    }));
                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.timeline,
                          size: 70.0,
                          color: Colors.orange,
                        ),
                        Text("Informasi Obat",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),

              Card(
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return Edukasi();
                    }));
                  },
                  splashColor: Colors.lightBlueAccent,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.school,
                          size: 70.0,
                          color: Colors.red,
                        ),
                        Text("Konsultasi",style: new TextStyle(fontSize: 17.0))
                      ],
                    ),
                  ),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}

